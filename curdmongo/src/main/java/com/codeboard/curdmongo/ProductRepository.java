package com.codeboard.curdmongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;


@RepositoryRestController(value="productdata",path="productdata")
public interface ProductRepository extends MongoRepository<Product, Integer> {

}
