package com.codeboard.curdmongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
public class InitializationComponent {
	
	@Autowired
	private ProductRepository productRepository;
	
	@PostConstruct
	private void init() {
		productRepository.deleteAll();
		
		Product product = new Product();
		product.setProductId(1);
		product.setName("Samsung");
		product.setCode("Samsung-Trio");
		product.setTitle("Samsung 12 INCH");
		product.setDescription("12 Inch touch screen");
		product.setImgUrlString("Samsung.jpg");
		product.setPrice(12000.00);
		productRepository.save(product);
		
		Product product2 = new Product();
		product2.setProductId(2);
		product2.setName("Apple");
		product2.setCode("Apple-Trio");
		product2.setTitle("Apple 12 INCH");
		product2.setDescription("12 Inch touch screen");
		product2.setImgUrlString("Apple.jpg");
		product2.setPrice(520000.00);
		productRepository.save(product2);
		
	}
}
