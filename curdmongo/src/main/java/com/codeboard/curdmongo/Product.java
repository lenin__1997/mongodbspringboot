package com.codeboard.curdmongo;

import org.springframework.data.annotation.Id;
import lombok.Data;

@Data
//@Setter
//@Getter
public class Product {
	
	@Id
	private Integer productId;
	private String name;
	private String code;
	private String title;
	private String description;
	private String imgUrlString;
	private Double price;
	
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImgUrlString() {
		return imgUrlString;
	}
	public void setImgUrlString(String imgUrlString) {
		this.imgUrlString = imgUrlString;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	
	
}
